.data
getA:  .asciiz     "Entra el nombre a multiplicar per 10: "
.text

li  $v0, 4 
la  $a0, getA
syscall

li  $v0, 5
syscall
move $s1, $v0


addi $s2,$zero,10
mul $s3, $s1, $s2
#mult $s1, $s2 (sense pseudoinstruccions)
#mflo $s3 (32 bits de menor pes)

li  $v0, 1
la  $a0, ($s3) # print dels 32 bits de menor pes
syscall