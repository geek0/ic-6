.data
primer: .asciiz "entra el primer nombre: "
segon: .asciiz "entra el segon nombre:"
suma: .asciiz "la suma es: "
.text
main: # el programa entra 2 nombres, si el segon no es igual a 0: (a = primer*segon) i si no es igual: (a = 100)
 li $v0, 4
 la $a0, primer #syscall per imprimir string primer
 syscall

 li $v0, 5 #syscall per llegir un enter
 syscall
 move $t0, $v0

 li $v0, 4
 la $a0, segon
 syscall

 li $v0, 5
 syscall
 move $t1, $v0
 add $s0, $zero, $zero
loop:
beq $t1, $zero, finish  # while(segon!=0){
 add $s0, $s0, $t0 #           a= a + primer;
 sub $t1, $t1, 1 #             segon--;
 j loop #                                }
finish:
addi $s0, $s0, 100 #           a = a + 100;
 add $v0, $s0, $zero    
 syscall 